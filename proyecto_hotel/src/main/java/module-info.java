module edu.espol.proyecto_hotel {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.base;

    opens edu.espol.proyecto_hotel to javafx.fxml;
    exports edu.espol.proyecto_hotel;
}