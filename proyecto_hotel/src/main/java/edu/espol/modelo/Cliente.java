/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.espol.modelo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Jandry J. Moreira Q
 */
public class Cliente implements Serializable{
    private String nombre;
    private String cedula;
    private ArrayList<Reserva>reservacion;
    
    private static final long serialVersionUID = 1L;
    
    public Cliente(String nombre, String cedula ){
        this.nombre=nombre;
        reservacion=new ArrayList<>();
        this.cedula=cedula;
        
    }
    
    public void clienteAddReserva(Reserva reserva){
        reservacion.add(reserva);
    }

    public ArrayList<Reserva> getReservacion() {
        return reservacion;
    }

    
    
    public String getNombre() {
        return nombre;
    }
    
    
}
