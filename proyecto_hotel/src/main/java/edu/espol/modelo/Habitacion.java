/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.espol.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.scene.control.DialogPane;
/**
 *
 * @author Jandry J. Moreira Q
 */
public class Habitacion implements Serializable {
    private int numero;
    private double precio;
    private String servicios;
    private String tipo;
    private boolean reservaState;  //true significa: ocupada/reservada, false significa: disponible
    private String estado;
    private Reserva reserva;
    
    private static final long serialVersionUID = 1L;
    
    public Habitacion(int numero, double precio, String servicios, String tipo ){
        this.numero=numero;
        this.precio=precio;
        this.servicios=servicios;
        this.tipo=tipo;
        this.reservaState=false;
        this.estado="Disponible";
    }
    
    public void setNewRoomDefaultTipe(){
        
    }
    
    public void setReserva(Reserva reserva){
        this.reserva=reserva;
    }
    
    public Reserva getReserva(){
        return reserva;
    }
    
    public void setReservaState(String s){
        if(s.equalsIgnoreCase("Disponible")){
            reservaState=false;
        }
        else if(s.equalsIgnoreCase("Ocupada")){
            reservaState=true;
        }
    }
    
    public int getNumHab(){
        return numero;
    }

    public String getEstado() {
        return estado;
    }

    public String getTipo() {
        return tipo;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public double getPrecio() {
        return precio;
    }
    
    

    @Override
    public String toString() {
        return "numero=" + numero + ", precio=" + precio + ", servicios=" + servicios + ", tipo=" + tipo;
    }
    
    
    
}
