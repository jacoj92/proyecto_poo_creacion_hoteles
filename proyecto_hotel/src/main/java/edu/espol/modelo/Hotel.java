/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.espol.modelo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Jandry J. Moreira Q
 */
public class Hotel implements Serializable {
    private String nombre;
    private String ciudad;
    private String direccion;
    private String telefono;
    private ArrayList<Habitacion>habitaciones;
    
    private static final long serialVersionUID = 1L;
    
    public Hotel(String nombre, String ciudad, String direccion, String telefono){
        this.nombre=nombre;
        this.ciudad=ciudad;
        this.direccion=direccion;
        this.telefono=telefono;
        habitaciones=new ArrayList<>();
    }
    
    
    public ArrayList<Habitacion> getHabitacionesDisponibles(){
        
        return null;
    }

    @Override
    public String toString() {
        return  nombre + " - " + ciudad ;
    }
    
    public void addHabitacion(Habitacion habitacion){
        habitaciones.add(habitacion);
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }
    
    public ArrayList<Habitacion> getHabitaciones(){
        return habitaciones;
    }
}
