/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.espol.modelo;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Jandry J. Moreira Q
 */
public class Reserva implements Serializable{
    
    private LocalDate fechaInicio; 
    private LocalDate fechaFin;
    private String clienteNomb;
    private int identificacion; 
    private String paisOrigen;
    private String formPago;
    
    private double valorReservacion;
    private ArrayList<Habitacion>habitaciones;
    private static int countReserv;  //contabiliza el numero de reservaciones     
    private Hotel hotel;
    
    public Reserva (LocalDate fechaInicio, LocalDate fechaFin, String clienteNomb, int identificacion, String paisOrigen, String formPago ){
        this.fechaInicio=fechaInicio;
        this.fechaFin=fechaFin;
        this.clienteNomb=clienteNomb;
        this.identificacion=identificacion;
        this.paisOrigen=paisOrigen;
        this.formPago=formPago;
        habitaciones=new ArrayList<>();
    }
    
    
    public void habitacionAddRserva(Habitacion habitacion){
       habitaciones.add(habitacion);
    }
    
    
    //metodo que devuelve el valor de la reservacion
    public double obtenerCostoRerservacion(){
        double precio=0;
        for (Habitacion h : habitaciones){
            precio+=h.getPrecio();
        }
        return precio;
    }

    public ArrayList<Habitacion> getHabitaciones() {
        return habitaciones;
    }
    
    public LocalDate getFechaInicio(){
        return fechaInicio;
    }
    
    public LocalDate getFechaFin(){
        return fechaFin;
    }
    
}
