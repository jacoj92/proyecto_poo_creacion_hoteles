/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.espol.proyecto_hotel;

import edu.espol.modelo.Cliente;
import edu.espol.modelo.Habitacion;
import edu.espol.modelo.Hotel;
import edu.espol.modelo.Reserva;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Jandry J. Moreira Q
 */
public class consulta_hoteles_habitacionesController implements Initializable {

    @FXML
    private ComboBox<Hotel> cboHoteles;
    @FXML
    private FlowPane fpHoteles;
    @FXML
    private DatePicker DPInicio;
    @FXML
    private DatePicker DPFin;
    @FXML
    private ComboBox<String> cboTipoHab;
    @FXML
    private Label lblAvisoHabitaciones;
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtIdentificacion;
    @FXML
    private TextField txtPaisOrigen;
    @FXML
    private TextField txtFormaPago;
    
   // private Button button;
    private RadioButton button;
    
    @FXML
    private Button btnCrearReservacion;
    
    private Cliente cliente;
    private Hotel hotel;
    private Habitacion habitacion;
    private Reserva reserva;
    private int count;
    
    @FXML
    private VBox btnReservacion;
    @FXML
    private Label lblAdvertencia;
    @FXML
    private ComboBox<String> cboEstadoHab;
    
    ToggleButton btnHotel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //actualizarEstadoHabitaciones();
        cargaHotelesCboBox();
        cargarEstadosHab();
    }    
    
    
    
    //metodo que lee los hoteles serializados (registrados) y los carga en el combobox
    public void cargaHotelesCboBox(){
        ObjectInputStream lecter = null;
        try {
            lecter = new ObjectInputStream(new FileInputStream("C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/hoteles_creados.txt"));
            ArrayList<Hotel>hotelesReg=(ArrayList)lecter.readObject();
            
            cboHoteles.getItems().addAll(hotelesReg);
                
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                lecter.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    //metodo que valida si la habitacion de un hotel tiene habitaciones creadas
    public boolean validarHabitaionesHotel(){
        
        ObjectInputStream lecter=null;
        String file= "C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/hotel_"+cboHoteles.getValue().toString();
        
        try {
            lecter = new ObjectInputStream(new FileInputStream(file));
            Hotel lecterHotel=(Hotel)lecter.readObject();
            if(lecterHotel.getHabitaciones()!=null){
                return true;
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            return false;
                
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return false;
    }
    
    public void cargarEstadosHab(){
        cboEstadoHab.getItems().add("Disponible");
        cboEstadoHab.getItems().add("Reservada");
        cboEstadoHab.getItems().add("Ocupada");
    }
    
    //metodo que carga de forma dinamica las habitaciones (botones) del hotel seleccionado 
    @FXML
    public void cargarHotelesFP(){
        fpHoteles.getChildren().clear();
        
        actualizarEstadoHabitaciones();
        
        //obtengo el tipo de habitacion seleccionado por el usuario en el combobox
        String tipoHab=cboTipoHab.getValue().toString();
        
        //valido que el hotel seleccionado tenga habitaciones creadas
        if( validarHabitaionesHotel() ){
            ObjectInputStream srlHotel= null;
            try {

                Hotel hotelSelec= cboHoteles.getValue();
                srlHotel = new ObjectInputStream(new FileInputStream("C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/hotel_"+hotelSelec));
                Hotel hotel=(Hotel)srlHotel.readObject();

                //llamada al metodo para ordenar de manera ascendente las habitaciones del hotel
                ArrayList<Habitacion>habitacionesSort=orderHabitacion(hotel.getHabitaciones());
                ToggleGroup group = new ToggleGroup();
                for (Habitacion h : habitacionesSort){
                    
                    //si el tipo de habitacion seleccionada es igual al tipo de habitacion registrada y estado consultado
                    //se muestra en el flowpane
                    if(tipoHab.equalsIgnoreCase(h.getTipo()) && cboEstadoHab.getValue().equalsIgnoreCase(h.getEstado()) ){
                        lblAdvertencia.setText("");
                        
                        /*
                        button=new Button();
                        button.setStyle("-fx-background-size:0");
                        button.setText(h.toString());
                        button.setWrapText(true);

                        //bloque que coloca imagen en el boton
                        Image imageHotel = new Image(getClass().getResourceAsStream("/edu/espol/proyecto_hotel/documentos/imagenes/hotel-icon-symbol-sign-vector.jpg"));
                        ImageView img = new ImageView(imageHotel);
                        img.setFitWidth(100);
                        img.setFitHeight(100);
                        button.setGraphic(img);
                        button.setGraphicTextGap(10);
                        button.setOnAction(eh -> obternerHabitacionBtn());*/

                        button = new RadioButton("RadioButton1");
                        button.setStyle("-fx-background-size:0");
                        button.setText(h.toString());
                        button.setWrapText(true);

                        Image imageHotel = new Image(getClass().getResourceAsStream("/edu/espol/proyecto_hotel/documentos/imagenes/hotel-icon-symbol-sign-vector.jpg"));
                        ImageView img = new ImageView(imageHotel);
                        img.setFitWidth(100);
                        img.setFitHeight(100);
                        button.setGraphic(img);
                        button.setGraphicTextGap(10);
                        button.setOnAction(eh -> obternerHabitacionBtn());
                        button.setOnAction(e1 -> hacerCheckIn());
                       // button.setOnAction(e2 -> hacerCheckOut());
                        
                        button.setToggleGroup(group);

                        //añado el boton al flowpane y establezco separacion entre los botones
                        fpHoteles.getChildren().add(button);
                        fpHoteles.setHgap(15);
                        fpHoteles.setVgap(15);
                               
                    }
                    
                }
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    srlHotel.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    //metodo que ordena las habitaciones de manera ascendente
    public ArrayList<Habitacion> orderHabitacion(ArrayList<Habitacion> habsHotel){
        //arraylist que almacenara los numeros de las habitaciones del hotel que se le pasa por parametro
        ArrayList<Integer>numHabitaciones=new ArrayList<>();
        //listado que almacenara lista de habitaciones ordenadas
        ArrayList<Habitacion>habitacionesSort=new ArrayList<>();
        
        //añado el numero de cada habitacion al arraylist numHabitaciones
        for( Habitacion h : habsHotel ){
            numHabitaciones.add(h.getNumHab());
        }
        
        //ordeno de forma ascendente el numero de las habitaciones
        Collections.sort(numHabitaciones);
        
        //recorro el listado ordenado y lo comparo con el listado de las habitaciones del hotel
        for(Integer numHab : numHabitaciones){
            for(Habitacion h : habsHotel){
                if(h.getNumHab()==numHab){
                    habitacionesSort.add(h);
                }
            }
        }
        
        return habitacionesSort;
    }
    
    //carga el tipo de habitaciones en el combobox, si el hotel tiene habitaciones creadas
    @FXML
    public void cargarTipoHabitacionesCboBox(){
        
        cboTipoHab.getItems().clear();
        lblAvisoHabitaciones.setText("");
        fpHoteles.getChildren().clear();
        ObjectInputStream lecter=null;
        String file= "C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/hotel_"+cboHoteles.getValue();
        
        try {
            lecter = new ObjectInputStream(new FileInputStream(file));
            Hotel lecterHotel=(Hotel)lecter.readObject();
            if(lecterHotel.getHabitaciones()!=null){
                
                cboTipoHab.getItems().add("Matrimonial");
                cboTipoHab.getItems().add("Suite");
                cboTipoHab.getItems().add("Doble");
                cboTipoHab.getItems().add("Triple");
                
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            lblAvisoHabitaciones.setText("ADVERTENCIA: El hotel seleccionado no contiene habitaciones registradas");
                
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    
    //metodo que crea la reserva y la serializa 
    @FXML
    public void crearReservacion(){
        
        ObjectOutputStream writerCliente=null;
        ObjectOutputStream writerHotel=null;
        
        ObjectInputStream lecterHotel=null;
        
        try {
            
            //obtengo datos para la creacion del cliente
            String nombreClie=txtNombre.getText();
            String cedula=txtIdentificacion.getText();
            
            //añado el nombre del cliente a la lista de clientes del hotel 
            App.huespedes.add(nombreClie);
            
            //creo el cliente
            cliente=new Cliente(nombreClie, cedula);
            
            //ruta donde se almacenara la informacion del cliente 
            String rootCliente= "C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/Clientes/"+cliente.getNombre();
            //si el cliente ya tiene reservaciones hechas
            if(new File(rootCliente).exists()){
                
                //deserializo la informacion del cliente
                ObjectInputStream lecterCliente=new ObjectInputStream(new FileInputStream(rootCliente));
                cliente=(Cliente)lecterCliente.readObject();

                //añado la reservacion a la lista de reservaciones del cliente 
                cliente.clienteAddReserva(reserva);
            }
            
            //si el cliente no tiene reservaciones hechas 
            else if (!new File(rootCliente).exists()){
                //serializo la informacion del cliente y la reserva
                writerCliente=new ObjectOutputStream(new FileOutputStream(rootCliente));
                writerCliente.writeObject(cliente);
            }
            count=0;
            
            //dado que el hotel existe en este punto, leo directamente su informacion
            String rootHotel= "C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/hotel_"+cboHoteles.getValue().toString();
            lecterHotel=new ObjectInputStream(new FileInputStream(rootHotel));
            Hotel hotel =(Hotel)lecterHotel.readObject();
            
            for(Habitacion h : hotel.getHabitaciones()){
                for(Habitacion hR : reserva.getHabitaciones()){
                    if(h.getNumHab()==hR.getNumHab()){
                        h.setReserva(reserva);
                    }
                }
            }
            
            //actualizo la informacion del hotel, serializandola nuevamente 
            writerHotel=new ObjectOutputStream(new FileOutputStream(rootHotel));
            writerHotel.writeObject(hotel);
            
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                writerCliente.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
    }
    
    
    //metodo que retorna los datos de una habitacion 
    public ArrayList<String> datosHabitacion(String hab){
        ArrayList<String> datos=new ArrayList<>();
        for(String s : hab.split(",")){
            int count=0;
            for(String st : s.split("=")){
                if(count==1){
                    datos.add(st);
                }
                count+=1;
            }
        }
        return datos;
    }
    
    public void obternerHabitacionBtn(){
        ObjectInputStream lecterHotel= null;
        ObjectOutputStream writerHotel=null;
        try {
            lblAdvertencia.setText("");
            //obtengo la habitacion
            fpHoteles.getChildren().size();
             RadioButton seleccionado=null;
            System.out.println(fpHoteles.getChildren().size());
            
            
            String habitacion="";
            for( int i=0; i<fpHoteles.getChildren().size();i++)
            {
                seleccionado= (RadioButton)fpHoteles.getChildren().toArray()[i];
                
               if(seleccionado.isSelected())
               {
                   System.out.println(seleccionado.getText()); 
                    habitacion=seleccionado.getText();
                }
            }
            
            ArrayList<String>datos=datosHabitacion(habitacion);
            Integer numero=Integer.parseInt(datos.get(0));
            Double precio=Double.parseDouble(datos.get(1));
            String servicios=datos.get(2);
            String tipo=datos.get(3);
            
            //creo la habitacion
            Habitacion hab=new Habitacion(numero, precio, servicios, tipo);
            System.out.println(numero+" "+precio+" "+servicios+" "+tipo);  
            
            //obtengo datos para la creacion de la reserva
            LocalDate fechaInicio=DPInicio.getValue();
            LocalDate fechaFin=DPFin.getValue();
            String nombreClie=txtNombre.getText();
            Integer identificacion=Integer.parseInt(txtIdentificacion.getText());
            String paisClie=txtPaisOrigen.getText();
            String formPago=txtFormaPago.getText();
            
            //creo la reservasion a nombre del cliente
            reserva=new Reserva(fechaInicio, fechaFin, nombreClie, identificacion, paisClie, formPago);
           // hab.setEstado("Disponible");
            
            //actualizo la informacion del hotel, deserializando su informacion
            String root= "C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/hotel_"+cboHoteles.getValue().toString();
            lecterHotel = new ObjectInputStream(new FileInputStream(root));
            Hotel hotel=(Hotel)lecterHotel.readObject();
            
            for(Habitacion h : hotel.getHabitaciones()){
                //extraigo la habitacion seleccionada
                if(h.getNumHab()==hab.getNumHab() ){
                    
                    
                    //si la habitacion ya ha sido añadida a la reserva
                    if(reserva.getHabitaciones().contains(hab) && h.getEstado().equals(("Reservada"))){

                        lblAdvertencia.setText("ADVERTENCIA: La habitacion ya ha sido reservada");
                    }

                    //si la habitacion NO ha sido añadida a la reserva
                    else if(!reserva.getHabitaciones().contains(hab) && hab.getEstado().equals("Disponible"))
                    {
                        //añado la habitacion a la reserva
                        h.setReserva(reserva);
                        h.setEstado("Reservada");
                        reserva.habitacionAddRserva(h);
                        
                        count+=1;
                        reserva.habitacionAddRserva(hab);
                        if (count==1){
                            
                            System.out.println(numero+" "+precio+" "+servicios+" "+tipo); 
                            lblAdvertencia.setText("La reservacion se realizo exitosamente! Ha reservado "+count+" habitacion! :)");
                        }
                        else{
                            lblAdvertencia.setText("La reservacion se realizo exitosamente! Ha reservado "+count+" habitaciones! :)");
                        }
                    }
                    else 
                    {
                        lblAdvertencia.setText("No se puede agregar la misma habitación en la reserva");
                    }
                }
            }
           
            //reescribo la informacion actualizada en un fichero serializado con la misma ruta del hotel 
            writerHotel=new ObjectOutputStream(new FileOutputStream(root));
            writerHotel.writeObject(hotel);
            
            seleccionado.setDisable(true);
            
            /*
            //establezco el nuevo estado de la habitacion, antes de añadirla a la lista de reservaciones
            hab.setEstado("Reservada");
            
            //añado la habitacion a la reserva
            reserva.habitacionAddRserva(hab);
            
            //añado la reservacion a la lista de reservaciones del cliente
            cliente.clienteAddReserva(reserva);  */
        }catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                lecterHotel.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
    }
    
    
    //metodo que actualiza el estado de las habitaciones de un hotel dependiendo de la fecha
    //de la reservacion, en el hotel consultado
    public void actualizarEstadoHabitaciones(){
        ObjectInputStream lecterHotel= null;
        ObjectOutputStream writerHotel=null;
        try {
            
            String root= "C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/hotel_"+cboHoteles.getValue().toString();
            lecterHotel = new ObjectInputStream(new FileInputStream(root));
            Hotel hotel=(Hotel)lecterHotel.readObject();
            
            for(Habitacion h : hotel.getHabitaciones()){
                //si la habitacion tiene reserva, actualizo su estado dependiendo de la fecha 
                if(h.getReserva()!=null){
                    Reserva reserva=h.getReserva();
                    //comparo la fecha de la reservacion, con la fecha actual
                    System.out.println("llegue"+reserva.getFechaInicio());
                    if(reserva.getFechaInicio().isAfter(LocalDate.now())){
                        h.setEstado("Disponible");
                    }
                    else if(reserva.getFechaInicio().isEqual(LocalDate.now()) || ( reserva.getFechaInicio().isBefore(LocalDate.now()) && reserva.getFechaFin().isAfter(LocalDate.now()) ) ){
                        if(h.getEstado().equalsIgnoreCase("Ocupada")){
                            h.setEstado("Ocupada");
                        }
                        else{
                            h.setEstado("Reservada");
                        }
                        
                    }
                    else{
                        h.setEstado("Ocupada");
                    }
                
                }
                
                //si la habitacion NO tiene reservas
                else{
                    h.setEstado("Disponible");
                }
            }
            
            //reescribo el archivo del hotel en la misma ruta
            writerHotel=new ObjectOutputStream(new FileOutputStream(root));
            writerHotel.writeObject(hotel);
            
            
            
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                lecterHotel.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void hacerCheckIn(){
      //  actualizarEstadoHabitaciones();
        
        System.out.println("aqui estoy :D");
        ObjectInputStream lecterHotel=null;
        ObjectOutputStream writerHotel=null;
        try {
            RadioButton seleccionado=null;
            System.out.println(fpHoteles.getChildren().size());
            String strhabitacion="";
            for( int i=0; i<fpHoteles.getChildren().size();i++)
            {
                seleccionado= (RadioButton)fpHoteles.getChildren().toArray()[i];
                
                if(seleccionado.isSelected())
                {
                    System.out.println(seleccionado.getText());
                    strhabitacion=seleccionado.getText();
                }
            }   
            
            ArrayList<String>datos=datosHabitacion(strhabitacion);
            Integer numero=Integer.parseInt(datos.get(0));
            Double precio=Double.parseDouble(datos.get(1));
            String servicios=datos.get(2);
            String tipo=datos.get(3);
            
            //creo la habitacion
            Habitacion hab=new Habitacion(numero, precio, servicios, tipo);
            
            //comparo la informacion de la habitacion seleccionada, con el registro serializado del hotel
            String root= "C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/hotel_"+cboHoteles.getValue().toString();
            lecterHotel = new ObjectInputStream(new FileInputStream(root));
            Hotel hotel =(Hotel)lecterHotel.readObject();
            
            for(Habitacion h : hotel.getHabitaciones()){
                if(hab.getNumHab()==h.getNumHab()){ //&& h.getEstado().equalsIgnoreCase("Reservada")){
                    h.setEstado("Ocupada");
                }
                else{
                    h.setEstado(h.getEstado());
                }
            }
            
            //actualizo la informacion del check in en el hotel
            writerHotel=new ObjectOutputStream(new FileOutputStream(root));
            writerHotel.writeObject(hotel);
            
           
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                lecterHotel.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void hacerCheckOut(){
        //actualizarEstadoHabitaciones();
        
        System.out.println("aqui estoy :D");
        ObjectInputStream lecterHotel=null;
        ObjectInputStream lecterCliente=null;
        ObjectOutputStream writerHotel=null;
        File fichero=null;
        
        try {
            
            //obtengo la informacion del boton seleccionado
            RadioButton seleccionado=null;
            System.out.println(fpHoteles.getChildren().size());
            String strhabitacion="";
            for( int i=0; i<fpHoteles.getChildren().size();i++)
            {
                seleccionado= (RadioButton)fpHoteles.getChildren().toArray()[i];
                
                if(seleccionado.isSelected())
                {
                    System.out.println(seleccionado.getText());
                    strhabitacion=seleccionado.getText();
                }
            }   
            
            ArrayList<String>datos=datosHabitacion(strhabitacion);
            Integer numero=Integer.parseInt(datos.get(0));
            Double precio=Double.parseDouble(datos.get(1));
            String servicios=datos.get(2);
            String tipo=datos.get(3);
            
            //creo la habitacion
            Habitacion hab=new Habitacion(numero, precio, servicios, tipo);
            
            String rootCliente= "C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/Cllientes/";
            
            
            //comparo la informacion de la habitacion seleccionada, con el registro serializado del hotel
            String rootHotel= "C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/hotel_"+cboHoteles.getValue().toString();
            lecterHotel = new ObjectInputStream(new FileInputStream(rootHotel));
            Hotel hotel =(Hotel)lecterHotel.readObject();
            
            for(Habitacion h : hotel.getHabitaciones()){
                if(hab.getNumHab()==h.getNumHab() && h.getEstado().equalsIgnoreCase("Ocupada")){
                    //la habitacion vuelve a estar disponible
                    h.setEstado("Disponible");
                    h.setReserva(null);
                    
                }
                else{
                    h.setEstado(h.getEstado());
                }
                
                //tambien recorro los huespedes para eliminar su reservacion 
                for(String nombreCliente : App.huespedes){
                    lecterCliente=new ObjectInputStream(new FileInputStream(rootCliente+nombreCliente));
                    Cliente cliente =(Cliente)lecterCliente.readObject();
                    for(Reserva reserva : cliente.getReservacion()){
                        for (Habitacion habitacionCliente : reserva.getHabitaciones()){
                            if(habitacionCliente.getNumHab()==hab.getNumHab()){
                                fichero=new File(rootCliente+nombreCliente);
                                fichero.delete();
                            }
                        }
                    }
                }
            }
            
            //actualizo la informacion del check in en el hotel
            writerHotel=new ObjectOutputStream(new FileOutputStream(rootHotel));
            writerHotel.writeObject(hotel);
            
            
           
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                lecterHotel.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
    }
    
    public void clearFP(){
        fpHoteles.getChildren().clear();
    }
}