/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.espol.proyecto_hotel;

import edu.espol.modelo.Habitacion;
import edu.espol.modelo.Hotel;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Jandry J. Moreira Q
 */
public class creacion_hoteles_habitacionesController implements Initializable {
    
    
    @FXML
    private TextField txtInfoHoteles;
    @FXML
    private Button btnCreacionHabitaciones;
    @FXML
    private ComboBox<Hotel> cboHoteles;
    @FXML
    private Label lblInfoHoteles;
    
    private ArrayList<String>documentsRead=new ArrayList<>();
    @FXML
    private TextField txtNumHab;
    @FXML
    private TextArea txtAreaService;
    @FXML
    private TextField txtPrice;
    @FXML
    private Label lblNumHab;
    @FXML
    private ComboBox<String> cboState;
    @FXML
    private ComboBox<String> cboComboTipo;
    
    /**
     * Initializes the controller class.
    */
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        cargarComboEstado();
        
        cargarCboTipo();
        
        String hotelesSerializados= "C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/hoteles_creados.txt";
        if (new File(hotelesSerializados).exists()){
            lecturaCargaHotelesReg();
        }
    }    
    
    @FXML
    public void crearHoteles(){
       ObjectInputStream lecter= null;
        
       //inicio del bloque try-cath para la lectura y carga de hoteles en el combobox por primera vez
       try {
            //si la lista no contiene el documento pasado por parametro; no se ha leido enteriormente
            if(!documentsRead.contains(txtInfoHoteles.getText())){
                documentsRead.add(txtInfoHoteles.getText());
                lblInfoHoteles.setText("");
                ObjectOutputStream srlALHoteles = null;
                String hotelesSerializados= "C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/hoteles_creados.txt";
                
                //inicio del bloque try-cath para la serializacion del arraylist que contiene los objetos de tipo hotel
                try{
                    String fileRoot = "edu/espol/proyecto_hotel/documentos/datos_hoteles/"+txtInfoHoteles.getText();
                    
                    //inicio del bloque try-catch para la lectura del archivo con la informacion de los hoteles
                    try(
                        BufferedReader r= new BufferedReader(new FileReader(ClassLoader.getSystemResource(fileRoot).getFile().replaceAll("%20", " ")));){
                        r.readLine();
                        String linea=r.readLine();
                        while( linea!=null){
                            String[]s=linea.split(";");
                            App.hoteles.add(new Hotel(s[0], s[1], s[2], s[3]));  //lleno la lista estatica de hoteles
                            linea=r.readLine();
                        }
                        
                    }catch(IndexOutOfBoundsException e){
                        System.out.println(e);
                    }catch (IOException ex) {
                        ex.printStackTrace();
                    } // fin del bloque try-catch para la lectura del archivo con la informacion de los hoteles
                    
                    //cboHoteles.getItems().addAll(App.hoteles);   // añade los objetos de tipo hotel, al combobox
                    
                    if( new File(hotelesSerializados).exists()){
                        //la clase FileOutputStream, ofrece un constructor con dos parametros
                        //en caso de querer agregar contenido al archivo, se escribe true
                        srlALHoteles = new ObjectOutputStream(new FileOutputStream("C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/"+"hoteles_creados.txt", true));
                        srlALHoteles.writeObject(App.hoteles);
                    }
                    else{                                     //este tipo de constructor de la clase FileOutputStream, crea el fichero en caso de no existir
                        srlALHoteles = new ObjectOutputStream(new FileOutputStream("C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/"+"hoteles_creados.txt"));
                        srlALHoteles.writeObject(App.hoteles);
                    }
                }catch(IOException ex){
                    ex.printStackTrace();
                } finally {
                    try {
                        srlALHoteles.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            else {
                lblInfoHoteles.setText("ATENCION: Documento ha sido leido previamente");
            }    
            
            //lectura de hoteles serializados 
            lecter = new ObjectInputStream(new FileInputStream("C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/hoteles_creados.txt"));
            ArrayList<Hotel>hotelesReg=(ArrayList)lecter.readObject();
            
            cboHoteles.getItems().addAll(hotelesReg);
            
        } catch (FileNotFoundException ex) { 
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                lecter.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    } 
    
    
    //metodo que lee los hoteles serializados (registrados) y los carga en el combobox
    public void lecturaCargaHotelesReg(){
        
        ObjectInputStream lecter = null;
        try {
            lecter = new ObjectInputStream(new FileInputStream("C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/hoteles_creados.txt"));
            ArrayList<Hotel>hotelesReg=(ArrayList)lecter.readObject();
            
            cboHoteles.getItems().addAll(hotelesReg);
                
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                lecter.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
    }
    
    
    //metodo que registra la creacion de una habitacion en un hotel seleccionado
    @FXML
    public void crearHabitaciones(){
        
        lblNumHab.setText("");
        ObjectOutputStream srlHotel = null;
        try {
            //extraigo el hotel seleccionado del combobox
            Hotel hotel= cboHoteles.getValue();
            
            //obtengo los atributos especificados para la habitacion creada
            int numero=Integer.parseInt(txtNumHab.getText());
            double precio= Double.parseDouble(txtPrice.getText());
            String servicios=txtAreaService.getText();
            String tipo=cboComboTipo.getValue().toString();
            String state= cboState.getValue().toString();
            
            //creo la habitacion
            Habitacion habitacion=new Habitacion(numero, precio, servicios, tipo);
            habitacion.setReservaState(state);
            habitacion.setEstado(state);
            
            
            //verifico que la habitacion no haya sido creada en el hotel seleccionado 
            if(verificarHabitacionHotel(hotel, numero)){
                String fileRoot= "C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/hotel_";
                if (!new File(fileRoot+hotel).exists()){
                    //añado la habitacion a la lista de habitaciones del hotel seleccionado
                    hotel.addHabitacion(habitacion);
                    srlHotel = new ObjectOutputStream(new FileOutputStream("C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/"+"hotel_"+hotel));
                    srlHotel.writeObject(hotel);
                }
                else{
                    //si el hotel ya ha sido registrado, lo deserializo y obtengo su listado de habitaciones, añado la nueva habitacion
                    //y finalmente reescribo el archivo actualizado 
                    ObjectInputStream lecterHotel;
                    try{
                        lecterHotel=new ObjectInputStream(new FileInputStream("C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/"+"hotel_"+hotel));
                        Hotel inHotel=(Hotel)lecterHotel.readObject();
                        
                        inHotel.addHabitacion(habitacion);
                        hotel=inHotel;
                        
                    }catch(IOException ex){
                        ex.printStackTrace();
                    }
                    
                    srlHotel = new ObjectOutputStream(new FileOutputStream("C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/"+"hotel_"+hotel));
                    srlHotel.writeObject(hotel);
                }
            }
        
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                srlHotel.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
    }
    
    public void cargarComboEstado(){
        cboState.getItems().add("Ocupada");
        cboState.getItems().add("Disponible");
        cboState.getItems().add("Reservada");
    }
    
    public void cargarCboTipo(){
        cboComboTipo.getItems().add("Matrimonial");
        cboComboTipo.getItems().add("Suite");
        cboComboTipo.getItems().add("Doble");
        cboComboTipo.getItems().add("Triple");
    }
    
    //metodo que verifica que la habitacion creada, no haya sido registrada previamente en el mismo hotel
    private boolean verificarHabitacionHotel(Hotel h, int num){
        
        ObjectInputStream lecterHotel;
        
        try {
            lecterHotel=new ObjectInputStream(new FileInputStream("C:/Users/Jandry J. Moreira Q/Documents/ProyectoPOO/proyecto_hotel/src/main/resources/edu/espol/proyecto_hotel/documentos/datos_hoteles/"+"hotel_"+h));
            Hotel inHotel=(Hotel)lecterHotel.readObject();
            for (Habitacion hab : inHotel.getHabitaciones()){
                if (hab.getNumHab()==num){
                    lblNumHab.setText("ADVERTENCIA: La habitacion ya ha sido "
                            +"registrada en este hotel");
                    return false;
                }
            }
            
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return true;
    }
}